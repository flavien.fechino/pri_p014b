from matplotlib import pyplot as plt
import cv2
import numpy as np
import glob
from PIL import Image
from math import *
from scipy.spatial import distance as dist
import argparse

# https://pyimagesearch.com/2014/07/14/3-ways-compare-histograms-using-opencv-python/


'''

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required = True,
	help = "C:/Users/agath/OneDrive/Bureau/PRI/ImagesTIFF_Rapport3Drugosite")
args = vars(ap.parse_args())
'''
index={}
images = {}

for path in glob.glob("C:/Users/agath/OneDrive/Bureau/PRI/i//*.tiff" ):
	
	
    #filename = path[path.rfind("/") +1:]
    filename = (path.split("/")[-1])
    #print(filename) #not the right filename
    img = Image.open(path)
    imarray = np.array(img)
    images[filename] = cv2.cvtColor(imarray,cv2.COLOR_BGR2RGB)

    hist = cv2.calcHist([imarray],[0,1,2],None,[16,16,16],[30,210,30,210,30,210])
    hist = cv2.normalize(hist, hist).flatten()
    index[filename] = hist
   




# METHOD #1: UTILIZING OPENCV
# Correlation : Computes the correlation between the two histograms
# Hellinger Bhattacharyya distance, used to measure the “overlap” between the two histograms.

# initialize OpenCV methods for histogram comparison
OPENCV_METHODS = (
    ("Correlation", cv2.HISTCMP_CORREL),
    #("Chi-Squared", cv2.HISTCMP_CHISQR),
    #("Intersection", cv2.HISTCMP_INTERSECT),
    ("Hellinger", cv2.HISTCMP_BHATTACHARYYA))
# loop over the comparison methods
for (methodName, method) in OPENCV_METHODS:
    # initialize the results dictionary and the sort
    # direction
    results = {}
    reverse = False
    # if we are using the correlation or intersection
    # method, then sort the results in reverse order
    if methodName in ("Correlation", "Intersection"):
        reverse = True

    # loop over the index
    for (k, hist) in index.items():
        # compute the distance between the two histograms
        # using the method and update the results dictionary
        d = cv2.compareHist(index["i\S200.tiff"], hist, method)
        results[k] = d
    # sort the results
    results = sorted([(v, k) for (k, v) in results.items()], reverse = reverse)

    # show the query image

    fig = plt.figure("Query")

    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(images["i\S200.tiff"])
    plt.axis("off")
    # initialize the results figure
    fig = plt.figure("Results: %s" % (methodName))
    fig.suptitle(methodName, fontsize = 20)
    # loop over the results
    for (i, (v, k)) in enumerate(results):
        # show the result
        
        plt.subplots_adjust(left=-0.1, right=0.99)
        ax = fig.add_subplot(1, len(images), i + 1)
        plt.imshow(images[k])
        k = (k.split(".")[0])
        ax.set_title("%s: %.2f" % (k, v))
        plt.axis("off")

    # show the OpenCV methods
    plt.show()








    # statistiques :
    # nombre de classe / valeur min et valeur max / largeur de classe 
    # valeur moyenne / écart-type
    # histogramme superposés de données intéressante
    # on the normalized histo - with gray levels

    # pièce la mieux attaqué qui sert de réf pour différencier au niveau des niveaux de gris avec le reste 
    # si y a des diff au niveau des histo c'est qu'il y a une diff d'attaque 
