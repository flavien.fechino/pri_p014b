from matplotlib import pyplot as plt
import argparse
import cv2
import numpy as np
import glob
from math import *

# maybe do it with a masked region around the ZOI ?
# https://pyimagesearch.com/2021/04/28/opencv-image-histograms-cv2-calchist/

# construct the argument parser and parse the argument

#ap = argparse.ArgumentParser()
#ap.add_argument("-i","--image",required=True, help = "path to input image")
#args = vars(ap.parse_args())

# !! quand il y a reflet et qu'on voit la polarisation, il y a des dendrites et donc l'attaque chimique est good !!

#test = cv2.imread(args["test"])

# TRY WITH THE FIRST BATCH

# we want to use the new dataset, so in order to do that first we need to be able to open tiff image / then, we need to get rid of the lab beacause there is no green screen behind it
test = cv2.imread('test2_q200_0.png')

img = []
for i in glob.glob("C:/Users/agath/OneDrive/Bureau/PRI/batch/*.png"):
	
	test = cv2.imread(i)
	img.append(test)
	# try to mask the upper side
	mask0 = np.zeros(test.shape[:2], dtype="uint8")
	cv2.rectangle(mask0,(500,150),(1600,652),255,-1)
	#(850,310),(1070,600),255,-1) 
	# pour le batch : (500,150),(1600,652)
	# pour test4 (600,150),(800,452)

	#display the masked region
	masked0 = cv2.bitwise_and(test,test,mask=mask0)
	plt.figure()
	plt.imshow(masked0)

	# extraction of background using LAB space

	lab = cv2.cvtColor(masked0, cv2.COLOR_BGR2LAB)
	a_channel = lab[:,:,1] 
	th = cv2.threshold(a_channel,90,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1] #threshold a-channel to isolate green background
	masked = cv2.bitwise_and(masked0, masked0, mask = th)    # contains dark background
	m1 = masked.copy()
	m1[th==0]=(255,255,255) # contains white background

	# we have some green reflection so we want to get rid of that 

	mlab = cv2.cvtColor(masked, cv2.COLOR_BGR2LAB)
	dst = cv2.normalize(mlab[:,:,1], dst=None, alpha=0, beta=255,norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
	threshold_value = 50
	dst_th = cv2.threshold(dst, threshold_value, 255, cv2.THRESH_BINARY_INV)[1]
	mlab2 = mlab.copy()
	mlab[:,:,1][dst_th == 255] = 120
	rtest = cv2.cvtColor(mlab, cv2.COLOR_LAB2BGR)
	rtest[th==0]=(255,255,255)



	rtest2 = cv2.cvtColor(rtest,cv2.COLOR_BGR2GRAY) # converts the image to grayscale
	#re change the value to 256


	hist = cv2.calcHist([rtest2],[0],mask0,[210],[30,210]) # calculate the histogram unnormalized


	"""
	# we want to see if we can find the two max of the image or their just too close so there is no real disparities
	hist = [val[0] for val in hist] # convert histogram to simple list
	indices = list(range(0,180)) # generate a list of indices
	s = [(x,y) for y,x in sorted(zip(hist,indices), reverse = True)] # descending sort-by-key with histogram
	highest = s[0][1]
	second_highest = s[1][1]
	i=1

	t = np.size(s,0)

	while abs(highest - second_highest) < 600 :
		second_highest = s[i+1][1]
		i+=1
		if (i == t) :
			print("No issues on the piece")
		else :
			print("Issues detected")

	# 5019, 4037

	print(s)
	#print(s[0][0])
	print(highest)
	print(second_highest)

	hist = cv2.calcHist([rtest2],[0],mask0,[210],[30,210])
	"""

	#max = np.argmax(hist)
	#print(max)

	fig = plt.figure()
	ax = fig.add_subplot(131)
	#plt.figure()
	plt.axis("off")
	plt.imshow(cv2.cvtColor(rtest2, cv2.COLOR_GRAY2RGB)) # dislay the image (matplotlib expects RGB image)

	# plot the histogram 

	ax = fig.add_subplot(132)
	#plt.figure()
	plt.title("Grayscale histogram unnormalized")
	plt.xlabel("Bins")
	plt.ylabel("# of Pixels")
	plt.plot(hist)
	plt.xlim([30,210]) # histogram goes from 0 to 256 for all the values of the pixel

	# valeur max de l'histogramme

	_,max,_,maxpos=cv2.minMaxLoc(hist)
	print("La valeur max est: ",max,"La position de la valeur max est: ",maxpos[1])

	# moyenne 

	s = 0
	s2=0
	mean = 0

	for i in range(0,209):
		s += hist[i][0]*i
		s2 += hist[i][0]

	mean=s/s2
	print("La moyenne de l'histogramme est: ",mean)
	plt.axvline(mean, color='k', linestyle='dashed', linewidth=1)
	min_ylim, max_ylim = plt.ylim()
	plt.text(mean*1.1, max_ylim*0.9, 'Mean: {:.2f}'.format(mean))


	# écart-type

	#etype = np.std(hist)
	#print(etype)

	s3=0
	s4=0
	for i in range(0,209):
		s3+=(i-mean)**2
		s4+=i
		
		

	etype=sqrt(s3/s4)
	print("L'écart-type de l'histogramme est: ",etype)

	
	# normalization of the histogram

	hist /= hist.sum()

	# plot the normalized histogram 

	ax = fig.add_subplot(133)
	#plt.figure()
	plt.title("Grayscale histogram normalized")
	plt.xlabel("Bins")
	plt.ylabel("# of Pixels")
	plt.plot(hist)
	plt.xlim([30,210]) # histogram goes from 0 to 256 for all the values of the pixel

	# color histogram to see the diff

	fig2 = plt.figure()
	ax = fig2.add_subplot(121)
	#plt.figure()
	plt.imshow(rtest)

	chanels = cv2.split(rtest) # split the image into color channel
	colors = ("b")

	ax = fig2.add_subplot(122)
	#plt.figure()
	plt.title("'Flattened' Color Histogram")
	plt.xlabel("Bins")
	plt.ylabel("# of Pixels")

	# loop over the image channels

	for (chanel, color) in zip(chanels, colors):

		# create a histogram for the current channel and plot it

		hist2 = cv2.calcHist([chanel], [0], mask0, [210], [30, 210]) #[0,256] de base
		plt.plot(hist2, color=color)
		plt.xlim([30, 210]) # we are going to have three histogram, one for the red, blue, green



	# we want an histogram 2D beacause we want to try using two channels at a time

	# green and blue channels

	fig = plt.figure()
	ax = fig.add_subplot(131)
	hist3 = cv2.calcHist([chanels[1], chanels[0]], [0, 1], mask0, [32, 32], [30, 210, 30, 210]) # waist to use 256 bins for a 2D histogram so we use 32
	p = ax.imshow(hist3, interpolation="nearest")
	ax.set_title("2D Color Histogram for G and B")
	plt.colorbar(p)

	#  green and red channels

	ax = fig.add_subplot(132)
	hist3 = cv2.calcHist([chanels[1], chanels[2]], [0, 1], mask0, [32, 32], [30, 210, 30, 210])
	p = ax.imshow(hist3, interpolation="nearest")
	ax.set_title("2D Color Histogram for G and R")
	plt.colorbar(p)

	#  blue and red channels
	ax = fig.add_subplot(133)
	hist3 = cv2.calcHist([chanels[0], chanels[2]], [0, 1], mask0, [32, 32], [30, 210, 30, 210])
	p = ax.imshow(hist3, interpolation="nearest")
	ax.set_title("2D Color Histogram for B and R")
	plt.colorbar(p)
	
	plt.show()



