from matplotlib import pyplot as plt
import cv2
import numpy as np
import glob
from PIL import Image
from math import *
from scipy.spatial import distance as dist


I=[]
index={}
images = {}
#for i in glob.glob("C:/Users/agath/OneDrive/Bureau/PRI/ImagesTIFF_Rapport3Drugosite/*.tiff"):
	
	#test = cv2.imread(i)
	#img.append(test)
#img = Image.open(i)
img = Image.open("s200.tiff")
imarray = np.array(img)

#I.append(imarray)



rtest2 = cv2.cvtColor(imarray,cv2.COLOR_BGR2GRAY)

hist = cv2.calcHist([rtest2],[0],None,[210],[30,210])

fig = plt.figure()
#ax = fig.add_subplot(131)
ax = fig.add_subplot(121)
plt.axis("off")
plt.imshow(cv2.cvtColor(rtest2, cv2.COLOR_GRAY2RGB)) # dislay the image (matplotlib expects RGB image)

# plot the histogram 

ax = fig.add_subplot(122)
#plt.figure()
plt.title("Grayscale histogram unnormalized")
plt.xlabel("Bins")
plt.ylabel("# of Pixels")
plt.plot(hist)
plt.xlim([30,210]) # histogram goes from 0 to 256 for all the values of the pixel

'''
# normalization of the histogram

hist /= hist.sum()

# plot the normalized histogram 

#ax = fig.add_subplot(133)
ax = fig.add_subplot(122)
#plt.figure()
plt.title("Grayscale histogram normalized")
plt.xlabel("Bins")
plt.ylabel("# of Pixels")
plt.plot(hist)
plt.xlim([30,210]) # histogram goes from 0 to 256 for all the values of the pixel
'''

# valeur max de l'histogramme

_,max,_,maxpos=cv2.minMaxLoc(hist)
print("La valeur max est: ",max,"La position de la valeur max est: ",maxpos[1])

# moyenne 

s = 0
s2=0
mean = 0

for i in range(0,209):
	s += hist[i][0]*i
	s2 += hist[i][0]

mean=s/s2
print("La moyenne de l'histogramme est :",mean)
plt.axvline(mean, color='k', linestyle='dashed', linewidth=1)
min_ylim, max_ylim = plt.ylim()
plt.text(mean*1.1, max_ylim*0.9, 'Mean: {:.2f}'.format(mean))


# écart-type

#etype = np.std(hist)
#print(etype)

s3=0
s4=0
for i in range(0,209):
	s3+=(i-mean)**2
	s4+=i
	
	

etype=sqrt(s3/s4)
print("L'écart-type de l'histogramme est:", etype)

# Comparison of the histograms

# METHOD #1: UTILIZING OPENCV
# initialize OpenCV methods for histogram comparison
OPENCV_METHODS = (
	("Correlation", cv2.HISTCMP_CORREL),
	("Chi-Squared", cv2.HISTCMP_CHISQR),
	("Intersection", cv2.HISTCMP_INTERSECT),
	("Hellinger", cv2.HISTCMP_BHATTACHARYYA))
# loop over the comparison methods
for (methodName, method) in OPENCV_METHODS:
	# initialize the results dictionary and the sort
	# direction
	results = {}
	reverse = False
	# if we are using the correlation or intersection
	# method, then sort the results in reverse order
	if methodName in ("Correlation", "Intersection"):
		reverse = True


plt.show()



# statistiques 


# tous les histogrammes ont 210 classes 

# average number of values per bin in your histogram - histrB.mean()



'''

# color histogram to see the diff

fig2 = plt.figure()
ax = fig2.add_subplot(121)
#plt.figure()
plt.imshow(imarray)

chanels = cv2.split(imarray) # split the image into color channel
colors = ("b")

ax = fig2.add_subplot(122)
#plt.figure()
plt.title("'Flattened' Color Histogram")
plt.xlabel("Bins")
plt.ylabel("# of Pixels")

# loop over the image channels

for (chanel, color) in zip(chanels, colors):

	# create a histogram for the current channel and plot it

	hist2 = cv2.calcHist([chanel], [0], None, [210], [30, 210]) #[0,256] de base
	plt.plot(hist2, color=color)
	plt.xlim([30, 210]) # we are going to have three histogram, one for the red, blue, green



# we want an histogram 2D beacause we want to try using two channels at a time

# green and blue channels

fig = plt.figure()
ax = fig.add_subplot(131)
hist3 = cv2.calcHist([chanels[1], chanels[0]], [0, 1], None, [32, 32], [30, 210, 30, 210]) # waist to use 256 bins for a 2D histogram so we use 32
p = ax.imshow(hist3, interpolation="nearest")
ax.set_title("2D Color Histogram for G and B")
plt.colorbar(p)

#  green and red channels

ax = fig.add_subplot(132)
hist3 = cv2.calcHist([chanels[1], chanels[2]], [0, 1], None, [32, 32], [30, 210, 30, 210])
p = ax.imshow(hist3, interpolation="nearest")
ax.set_title("2D Color Histogram for G and R")
plt.colorbar(p)

#  blue and red channels
ax = fig.add_subplot(133)
hist3 = cv2.calcHist([chanels[0], chanels[2]], [0, 1], None, [32, 32], [30, 210, 30, 210])
p = ax.imshow(hist3, interpolation="nearest")
ax.set_title("2D Color Histogram for B and R")
plt.colorbar(p)
plt.show()
'''

# statistiques :
# nombre de classe / valeur min et valeur max / largeur de classe 
# valeur moyenne / écart-type
# histogramme superposés de données intéressante
# on the normalized histo - with gray levels

# pièce la mieux attaqué qui sert de réf pour différencier au niveau des niveaux de gris avec le reste 
# si y a des diff au niveau des histo c'est qu'il y a une diff d'attaque 
