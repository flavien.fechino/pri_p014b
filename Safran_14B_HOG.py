#!/usr/bin/env python
# coding: utf-8

# In[1]:


from matplotlib import pyplot as plt
import argparse
import cv2
import numpy as np
import glob
from PIL import Image
from skimage.feature import hog
from skimage import data, color, exposure


# In[ ]:


#HOG : Histogram of oriented gradients : source of the code : https://stackoverflow.com/questions/44955480/python-histogram-of-oriented-gradients
#Documentation for the HOG fonction : https://scikit-image.org/docs/stable/api/skimage.feature.html#skimage.feature.hog
img = []
for i in glob.glob(".../*.png"): #Path to the folder of ROIs
    test = cv2.imread(i)
    img.append(test)
    title = "pièce " + str(i.split("/")[-1])  
    image = cv2.cvtColor(test, cv2.COLOR_BGR2GRAY)
    fd, hog_image = hog(image, orientations=9, pixels_per_cell=(32,32),cells_per_block=(1, 1),visualize=True)
    # Rescale histogram for better display
    hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.5))
    fig, ax = plt.subplots(1, 2, figsize=(20, 10), sharex=True, sharey=True)
    ax[0].axis('off')
    ax[0].imshow(image, cmap=plt.cm.gray)
    ax[0].set_title(title)
    ax[1].axis('off')
    ax[1].imshow(hog_image, cmap=plt.cm.gray)
    ax[1].set_title('Histogram of Oriented Gradients')
    plt.show


# In[ ]:


# L*a*b color space 
img = []
for i in glob.glob("C:/Users/fiass/Desktop/Fise3/Pri/rois/rois/*.png"):
    test = cv2.imread(i)
    img.append(test)
    title = "pièce " + str(i.split("/")[-1])  
    print(title)
    fig = plt.figure()
    plt.imshow(test)
    plt.axis("off")
    plt.show()
    # Convert the image to the L*a*b color space
    image_lab = cv2.cvtColor(test, cv2.COLOR_RGB2LAB)
    # Get the L*, a*, and b* channels as separate images
    l_channel, a_channel, b_channel = cv2.split(image_lab)
    # Create a histogram for each channel
    l_hist = cv2.calcHist([l_channel], [0], None, [256], [0, 256])
    a_hist = cv2.calcHist([a_channel], [0], None, [256], [0, 256])
    b_hist = cv2.calcHist([b_channel], [0], None, [256], [0, 256])
    # Plot the histograms 
    plt.figure()
    plt.title("L* Histogram")
    plt.xlabel("Value")
    plt.ylabel("Nbr of pixels")
    plt.plot(l_hist)
    # To plot the other channels 
    #plt.figure()
    #plt.title("a* Histogram")
    #plt.xlabel("Value")
    #plt.ylabel("Nbr of pixels")
    #plt.plot(a_hist)

    #plt.figure()
    #plt.title("b* Histogram")
    #plt.xlabel("Value")
    #plt.ylabel("Nbr of pixels")
    #plt.plot(b_hist)

    plt.show()

